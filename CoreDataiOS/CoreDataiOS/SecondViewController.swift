//
//  SecondViewController.swift
//  CoreDataiOS
//
//  Created by Roger on 2/2/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    var persona:Person?
    private let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        addressLabel.text = persona?.name
        phoneLabel.text = persona?.phone
    }

    
    @IBAction func deleteButtonPressed(_ sender: Any) {
        managedObjectContext.delete(persona!)
        do{
            try managedObjectContext.save()
            navigationController?.popViewController(animated: true)
        } catch {
            print("Error al eliminar")
        }
    }
    
}
