//
//  PersonTableViewCell.swift
//  CoreDataiOS
//
//  Created by Roger on 2/2/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameCell: UILabel!
    @IBOutlet weak var telephoneCell: UILabel!
    @IBOutlet weak var addressCell: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func fillData(person:Person){
        print("Llenando los datos")
        nameCell.text = person.name
        telephoneCell.text = person.phone
        addressCell.text = person.address
    }
}
