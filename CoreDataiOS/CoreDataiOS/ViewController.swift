//
//  ViewController.swift
//  CoreDataiOS
//
//  Created by Roger on 26/1/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    var persona:Person?
    var personasArray2:[Person] = []
    
    private let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    @IBAction func saveButtonPressed(_ sender: Any) {
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: managedObjectContext)
        
        let person = Person(entity: entityDescription!, insertInto: managedObjectContext)
        
        person.name = nameTextField.text
        person.address = addressTextField.text
        person.phone = phoneTextField.text
        do{
            try managedObjectContext.save()
            clearFields()
        }catch{
            print("Error al guardar")
        }
        
        
    }
    
    func clearFields(){
        nameTextField.text = ""
        addressTextField.text = ""
        phoneTextField.text = ""
    }
    

    @IBAction func findButtonPressed(_ sender: Any) {
        if nameTextField.text == ""{
            fetchAll()
        } else{
            fetchByName()
        }
    }
    
    func fetchAll(){
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        do{
            let results = try managedObjectContext.fetch(request)
            
            for p in results{
                let person = p as! Person
            }
             personasArray2 = results
            
        }catch{
            print("Error al consultar todos los datos")
        }
    }
    
    func fetchByName(){
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        let pred = NSPredicate(format: "name = %@", nameTextField.text!)
        request.predicate = pred
        do{
            let results = try managedObjectContext.fetch(request)
            if results.count > 0{
                persona = results[0] as! Person
                nameTextField.text = persona?.name
                addressTextField.text = persona?.address
                phoneTextField.text = persona?.phone
                
                performSegue(withIdentifier: "segue", sender: self)
            } else {
                addressTextField.text = ""
                phoneTextField.text = ""
                fetchAll()
                performSegue(withIdentifier: "segueSinResultados", sender: self)
            }
        }catch{
            print("Error al consultar por nombre")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segue"){
            let destination = segue.destination as! SecondViewController
            destination.persona = persona
        }
        if(segue.identifier == "segueSinResultados"){
            let destination = segue.destination as! TableViewController
            destination.personasArray = personasArray2
        }
    }
}
